## Implementación del CI/CD

Con la finalidad de implementar el CI/CD haciendo uso de GitLab para ello. este proyecto sirve como plantilla (template) destinado solo al CI/CD. Los stages implementados son:

**build  :** Script que permite la compilación del proyecto con la finalidad de saber si se ha roto o no el ciclo de compilación del mismo.

**test   :** Script implementado donde se ejecutaran los test unitarios para aprobar que el cambio realizado no afecto el comportamiento que ha de tener la API.

**build-docker   :** Script que permite construir el contenedor que se seguira utilizando en el despliegue y lo almacenara en el Registry de GCP destinado para este proyecto.

**release   :** Estos scripts (para desarrollo y producción) permiten realizar el 'retagging' de las imagenes subidas y ubicarlas correctamente segun el ambiente donde se vaya a deployar. Se sigue haciendo uso del Registry de GCP para esta actividad. 

**deploy   :** El despligue de software como su nombre indica, permite interactuar y crear los POD correspondientes en el cluster indicado para la actividad. En este Job de igual manera se implementa el HPA (Horizontal Pod Autoscaler).
